function arcane_filter_teams(gid) {
  var leftteams = 0;
  var testz = gid.toString();
  var holderz;
  jQuery('.join_team').each(function (i) {
    var test = jQuery(this).data('games').indexOf(testz);
    if (test < 0) {
      jQuery(this).fadeOut('fast');
    } else {
      jQuery(this).fadeIn('fast');
      leftteams ++;
      holderz = this;
    }

  });
  if (leftteams == 1) {
    jQuery(holderz).click();
    return false;
  } else if (leftteams === 0) {
    //tusi
    NotifyMe(settingsTournaments.no_teams_play, "error");
    return false;
  } else {
    return true;
  }
}


jQuery(document).ready(function ($) {
var CTournament = 0;
if(!settingsTournaments.tournamentpage){
	var lastRunJoin = null;
    var lastRunJoined = null;
      jQuery('.tlistjoin').one('click', '.join_tournament', function(e) {
        e.preventDefault();
        CTournament = jQuery(this).data('tid');
        jQuery(this).html('');
        jQuery(this).html('<img src='+settingsTournaments.loader+' />');
        var KeepThis = jQuery(this);

        if (jQuery(this).data('ttype') == 'team') {
          jQuery('.join_team').on('click', function(e) {
            if (lastRunJoin === null || new Date().getTime() - lastRunJoin > 500) {
              NotifyMe(settingsTournaments.joining_tournament, "information");
            }
             lastRunJoin = new Date().getTime();

    				jQuery.post( ajaxurl , { action: "join_tournament", tid: CTournament, pid:jQuery(this).data('team_id') }, function (data) {

              KeepThis.fadeOut('slow', function() {
                if (lastRunJoined === null || new Date().getTime() - lastRunJoined > 500) {
                  NotifyMe(settingsTournaments.tournament_joined, "success");
                }
                 lastRunJoined = new Date().getTime();

                KeepThis.html('<i class="fa fa-trophy"></i> ' + settingsTournaments.leave);

                KeepThis.removeClass('join_tournament');
                KeepThis.addClass('leave_tournament');
                KeepThis.fadeIn('slow');
              });
    				});
            	});
              if (arcane_filter_teams(jQuery(this).data('gid'))) {
                jQuery('#TeamChooserModalFooter').modal('show');
              }
        } else {
          NotifyMe(settingsTournaments.joining_tournament, "information");
          jQuery(this).html('');
          jQuery(this).html('<img src='+settingsTournaments.loader+' />');

          jQuery.post( ajaxurl , { action: "join_tournament", tid: CTournament, pid: settingsTournaments.c_id }, function (data) {
            KeepThis.fadeOut('slow', function() {
              NotifyMe(settingsTournaments.tournament_joined, "success");
              KeepThis.html('<i class="fa fa-trophy"></i> ' + settingsTournaments.leave);
              KeepThis.removeClass('join_tournament');
              KeepThis.addClass('leave_tournament');
              KeepThis.fadeIn('slow');
            });
          });
        }


      });

      jQuery('.tlistjoin').on('click', '.leave_tournament',function() {
        NotifyMe(settingsTournaments.leaving_tournament, "information");
        jQuery(this).html('');
        jQuery(this).html('<img src='+settingsTournaments.loader+' />');
        var KeepThis = jQuery(this);
        jQuery.post( ajaxurl , { action: "leave_tournament", tid: jQuery(this).data('tid'), pid: settingsTournaments.c_id }, function (data) {
          KeepThis.fadeOut('slow', function() {
            NotifyMe(settingsTournaments.tournament_left, "error");
            KeepThis.html('<i class="fa fa-trophy"></i> ' + settingsTournaments.join);
            KeepThis.removeClass('leave_tournament');
            KeepThis.addClass('join_tournament');
            KeepThis.fadeIn('slow');
          });
        });
      });
}




});

if (settingsTournaments.started == false || (settingsTournaments.current_user_can && settingsTournaments.admin_edit) ) {

	jQuery(document).ready(function($) {

	    var dateStart = settingsTournaments.tournament_starts_unix;
	    var timezone = settingsTournaments.tournament_timezone;
	    var tStart = moment.unix(dateStart).tz(timezone);

		/*mozda ce trebati
	    if(!settingsTournaments.editable) {
	        jQuery('#tournament_starts').html(tStart.format(settingsTournaments.moment_format));
	    }
	    */

	    jQuery('#tournament_countdown').countdown(tStart.toDate(), function(event) {
	        jQuery(this).html(event.strftime('%D '+settingsTournaments.days+' %H:%M:%S'));
	    });

	});
}

if (settingsTournaments.editable === '1') {

	jQuery(document).ready(function() {

		var options = {pk: settingsTournaments.data_pid, url: ajaxurl, mode: 'inline',onblur: "ignore"};

		jQuery('#tournament_title').editable(options);

		jQuery('#tournament_description').editable({
	        pk: settingsTournaments.data_pid,
	        url: ajaxurl,
	        placement: 'bottom',
	        title: 'Enter comments',
	        showbuttons: 'bottom',
	        onblur: 'ignore',
	        wysihtml5: {
	          "image": false,
	          "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
	          "emphasis": true, //Italics, bold, etc. Default true
	          "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
	          "html": true, //Button which allows you to edit the generated HTML. Default false
	          "link": false, //Button to insert a link. Default true
	       },
       });

       jQuery('#tournament_max_participants').editable(options);

	   jQuery('#tournament_server').editable(options);

	   jQuery(".game_chooser").on('click', function(e) {
	   	jQuery("#GameChooserModal").modal('show');
	   });

	   jQuery("#tournament_starts").editable({
            pk: settingsTournaments.data_pid,
            url: ajaxurl,
            format: settingsTournaments.moment_format,
            template: settingsTournaments.moment_format,
            combodate: {
                minYear: settingsTournaments.this_year,
                maxYear: parseInt(settingsTournaments.this_year) + 10,
                format: settingsTournaments.moment_format,
                minuteStep: 1
           }
        });

        jQuery("#tournament_timezone").editable({
            pk: settingsTournaments.data_pid,
            url: ajaxurl,
            prepend: settingsTournaments.please_choose,
            value: settingsTournaments.timezone_value,
            mode: "inline",
            source: settingsTournaments.timezone_mode,
        });

        jQuery('.prize_text').editable({
            pk: settingsTournaments.data_pid,
            onblur: 'ignore',
            url: ajaxurl,
            mode: 'inline',
            type: 'text',
            title: settingsTournaments.enter_prize
        });

        var WorkingAddPrize = false;
		var CurrentPrizes = parseInt(settingsTournaments.prizes);
        jQuery("#add_prize").on('click', function(e) {
            if (WorkingAddPrize == true) {
                NotifyMe(settingsTournaments.adding_prize, "information");
            } else {
            WorkingAddPrize = true;
            var tempprizes = CurrentPrizes;
            CurrentPrizes = CurrentPrizes + 1;
            var KeepThis = jQuery(this);
            var TempText = jQuery(this).html();
            var loader = settingsTournaments.loader;
            jQuery(this).html('');
            jQuery(this).html('<img src='+loader+' />');
            jQuery.post( ajaxurl , { action: "get_next_prize_template", currentprize: tempprizes }, function (data) {
                //get prize template
                KeepThis.html('');
                KeepThis.html(TempText);
                jQuery('#prizes_table tr:last').before(data);
                jQuery('.prize_text').editable({
                    pk: settingsTournaments.data_pid,
                    url: ajaxurl,
                    mode: 'inline',
                    onblur: 'ignore',
                    type: 'text',
                    title: settingsTournaments.enter_prize
                });
                WorkingAddPrize = false;
            });
            }

        });

		var CurrentPrizes = parseInt(settingsTournaments.prizes);
        jQuery("#remove_prize").on('click', function(e) {

            jQuery.post( ajaxurl , { action: "remove_prize_template", pk: settingsTournaments.data_pid}, function (data) {
                //get prize template
                CurrentPrizes = CurrentPrizes - 1;
                if (CurrentPrizes < 0) {
                    CurrentPrizes = 0;
                }
                if (CurrentPrizes >= 0) {
                    jQuery('#prizes_table tr:last').prev().slideUp("fast", function() {
                        jQuery('#prizes_table tr:last').prev().remove();
                    });
                }

            });
        });

		jQuery('#regulations_tabs').on('click', 'li', function() {
            jQuery("#regulations_tab_a_" + jQuery(this).data('tabtarget')).tab('show');
        });
		var Regulations = parseInt(settingsTournaments.regulations_num);
        jQuery("#add_new_regulation").on('click', function(e) {
            var TempText = "";
            var TempText2 = '';
            if (Regulations == 0) {
                jQuery('#regulations_containters').slideDown(500);
                jQuery('#regulations_tabs').slideDown(500);
                TempText = ' class="active"';
                TempText2 = 'in active';
            }
            setTimeout(function(){
                jQuery('#regulations_tabs').append('<li role="presentation" '+ TempText + ' data-tabtarget ="'+ Regulations +'"  id="regulations_tab_li_'+ Regulations +'"><a href="#regulations_tab_'+ Regulations +'" id="regulations_tab_a_'+ Regulations +'" aria-controls="#regulations_tab_'+ Regulations +'" role="tab" data-toggle="tab" style="display:none;"><a data-type="text" data-regulation_id="'+ Regulations +'" id="regulations_tab_name_'+ Regulations +'">'+ settingsTournaments.regulation_title +'<i class="fas fa-cog" aria-hidden="true"></i></a></a></li>'); //add new li
                jQuery('#regulations_containters div:last').before('<div role="tabpanel" class="tab-pane fade '+ TempText2 +'" id="regulations_tab_'+ Regulations +'"><span class="regulation_text"><div id="regulations_tab_content_'+ Regulations +'"  data-type="wysihtml5" >'+ settingsTournaments.regulation_text +'<i class="fas fa-cog" aria-hidden="true"></i></div></span></div>');
                var TempString = '#regulations_tab_name_'+ Regulations;
                jQuery(TempString).editable(options);

                var options3 = {pk: settingsTournaments.data_pid, url: ajaxurl};
                var TempString3 = '#regulations_tab_content_'+ Regulations;
                jQuery(TempString3).editable(options3);
                Regulations = Regulations + 1;
        	}, 600);
        });
		var Regulations = parseInt(settingsTournaments.regulations_num);
		jQuery("#remove_regulation").on('click', function(e) {
            jQuery.post( ajaxurl , { action: "remove_regulation_template", pk: settingsTournaments.data_pid}, function (data) {
                //get prize template

                Regulations = Regulations - 1;
                if (Regulations <= 0) {
                    Regulations = 0;
                    jQuery('#regulations_containters').slideUp(500);
                    jQuery('#regulations_tabs').slideUp(500);
                }
                setTimeout(function(){
                jQuery("#regulations_tab_" + Regulations).remove();
                jQuery("#regulations_tab_li_" + Regulations).remove();
            	}, 600);
            });
        });

        jQuery(document).on( "click", '.map_selector', function() {
            var keepmapid = jQuery(this).data('map-id');
            if (jQuery(this).hasClass('selected_map')) {
                //deselect map
                jQuery(this).removeClass('selected_map');
                jQuery.post( ajaxurl , { action: "change_selected_maps", tid: settingsTournaments.data_pid, mapid: keepmapid, whatdo: "remove" }, function (data) {
                    //map deselected
                });
            } else {
                jQuery(this).addClass('selected_map');
                jQuery.post( ajaxurl , { action: "change_selected_maps", tid: settingsTournaments.data_pid, mapid: keepmapid, whatdo: "add" }, function (data) {
                    //map selected
                });
            }
        });


        jQuery('#tournament_starts').on('save', function(e, params) {

            var dateStart = params.newValue.unix();
            var tStart = moment.unix(dateStart);
            jQuery(this).attr('data-unix',dateStart);

            jQuery('#tournament_countdown').countdown(tStart.toDate(), function(event) {
                jQuery(this).html(event.strftime('%D '+settingsTournaments.days+' %H:%M:%S'));
            });

            jQuery.post( ajaxurl , { action: "arcane_update_tournament_data", pk: settingsTournaments.data_pid, name: 'tournament_starts_unix', value: dateStart }, function (data) {});
        });


        jQuery("#tournament_format").editable({
            pk: settingsTournaments.data_pid,
            url: ajaxurl,
            onblur: "ignore",
            prepend: settingsTournaments.please_choose,
            value: settingsTournaments.slug_value,
            mode: "inline",
            source: settingsTournaments.slug_mode,
        });

		jQuery("#tournament_format").on('save', function(e, params) {
        	jQuery("#tournament_format").attr('data-format',params.newValue );
        	if(params.newValue == 'ladder'){
        	    jQuery("#tournament_game_frequency").closest( "li" ).hide();
            }else {
                jQuery("#tournament_game_frequency").closest( "li" ).show();
        	}
        });

		jQuery("#tournament_games_format").editable({
            pk: settingsTournaments.data_pid,
            url: ajaxurl,
            mode: "inline",
            onblur: "ignore",
            prepend: settingsTournaments.please_choose,
            source: [
                {value: "bo1", text: settingsTournaments.best_of + " 1"},
                {value: "bo3", text: settingsTournaments.best_of + " 3"},
                {value: "bo5", text: settingsTournaments.best_of + " 5"}
            ]
        });

        jQuery("#tournament_platform").editable({
            pk: settingsTournaments.data_pid,
            url: ajaxurl,
            mode: "inline",
            onblur: "ignore",
            prepend: settingsTournaments.please_choose,
            source: [
                {value: "ps", text: settingsTournaments.ps},
                {value: "pc", text: settingsTournaments.pc},
                {value: "xbox", text: settingsTournaments.xbox},
                {value: "wii", text: settingsTournaments.wii},
                {value: "nin", text: settingsTournaments.nin},
                {value: "cross", text: settingsTournaments.cross}
            ]
        });

        jQuery("#tournament_game_frequency").editable({
            pk: settingsTournaments.data_pid,
            url: ajaxurl,
            mode: "inline",
            onblur: "ignore",
            prepend: settingsTournaments.please_choose,
            source: [
                {value: "15 minutes", text: settingsTournaments.fifteen_minutes},
                {value: "30 minutes", text: settingsTournaments.thirty_minutes},
                {value: "60 minutes", text: settingsTournaments.sixty_minutes},
                {value: "1", text: settingsTournaments.daily},
                {value: "2", text: settingsTournaments.two_days},
                {value: "3", text: settingsTournaments.third_days},
                {value: "4", text: settingsTournaments.four_days},
                {value: "5", text: settingsTournaments.five_days},
                {value: "6", text: settingsTournaments.six_days},
                {value: "7", text: settingsTournaments.seven_days},
                {value: "14", text: settingsTournaments.fourteen_days},
                {value: "30", text: settingsTournaments.monthly}
            ]
        });


		jQuery("#tournament_frequency").editable({
            pk: settingsTournaments.data_pid,
            url: ajaxurl,
            mode: "inline",
            onblur: "ignore",
            prepend: settingsTournaments.please_choose,
            source: [
                {value: "daily", text: settingsTournaments.daily},
                {value: "weekly", text: settingsTournaments.weekly},
                {value: "monthly", text: settingsTournaments.monthly},
                {value: "yearly", text: settingsTournaments.yearly},
            ]
        });

        jQuery("#game_modes").editable({
            pk: settingsTournaments.data_pid,
            url: ajaxurl,
            mode: "inline",
            onblur: "ignore",
            prepend: settingsTournaments.please_choose,
            source: settingsTournaments.game_modes
        });

        jQuery("#tournament_contestants").editable({
            pk: settingsTournaments.data_pid,
            url: ajaxurl,
            mode: "inline",
            onblur: "ignore",
            prepend: settingsTournaments.please_choose,
            source: [
                {value: "team", text: settingsTournaments.teams},
                {value: "user", text: settingsTournaments.users},
            ]

        });

        jQuery("#tournament_contestants").on('save', function(e, params) {
        	jQuery("#tournament_contestants").attr('data-contestants',params.newValue+'s');
            jQuery.post( ajaxurl , { action: "blank_tournament", tid: settingsTournaments.data_pid }, function (data) {
                jQuery(".competitor_listing").each(function(index) {
                    jQuery(this).fadeOut('fast', function(e) {
                        jQuery(this).remove();
                    });
                });
            });
        });


        jQuery(".change_game").on('click', function(e) {

            jQuery("#GameChooserModal").modal('hide');
            var keepimg = jQuery(this).data('imagesrc');
            var keepmaps = jQuery(this).data('maps');
            var keeptitle = jQuery(this).data('gametitle');
            var keepbanner  = jQuery(this).data('banner');
            var keepgameid =  jQuery(this).data('game_id');
            jQuery.post( ajaxurl , { action: "arcane_update_tournament_data", pk: settingsTournaments.data_pid, name: 'game', value: jQuery(this).data('gametitle') }, function (data) {

                jQuery("#game_chooser_img").fadeOut( "slow", function() {
                    jQuery("#game_chooser_img").attr('src',keepimg);
                    jQuery("#game_chooser_img").fadeIn("slow");
                });
                jQuery("#game_name").fadeOut( "slow", function() {
                    jQuery("#game_name").html(keeptitle);
                    jQuery("#game_name").fadeIn("slow");
                });
                var tmpImg = new Image() ;
                tmpImg.src = keepbanner;

                tmpImg.onload = function() {
                    jQuery("#header_background_image").fadeOut("slow", function () {
                        jQuery("#header_background_image").css("background", "url(" + keepbanner + ")  no-repeat center center");
                        jQuery("#header_background_image").fadeIn("slow");
                    });
                } ;



                if (keepmaps > 1)  {
                    //games with maps
                    if (!(jQuery('.tbmaps').is(':visible'))) {
                        jQuery.post( ajaxurl , { action: "tournament_load_maps",  game_id: keepgameid }, function (data) {
                            jQuery("#maps_holder").html(data);
                            jQuery(".tbmaps").slideDown("slow");
                        });
                    } else {
                        jQuery(".tbmaps").slideUp("slow", function () {
                            jQuery.post( ajaxurl , { action: "tournament_load_maps",  game_id: keepgameid }, function (data) {
                                jQuery("#maps_holder").html(data);
                                jQuery(".tbmaps").slideDown("slow");
                            });
                        });
                    }
                } else {
                    jQuery(".tbmaps").slideUp("slow");
                }
            });

        });
	});

}


jQuery(document).ready(function(e) {
    jQuery('.single-tournament .members-list .join_team').on('click', function(e) {
        NotifyMe(settingsTournaments.joining_tournament, "information");
        jQuery.post( ajaxurl ,
            { action: "join_tournament",
            tid: settingsTournaments.data_pid,
            pid:jQuery(this).data('team_id') }, function (data) {
            location.reload();
        });
    });

     jQuery('#leave_tournament').on('click', function(e) {
        var loader = settingsTournaments.loader;
        jQuery(this).html('');
        jQuery(this).html('<img src='+loader+' />');
        NotifyMe(settingsTournaments.leaving_tournament, "information");
        jQuery.post( ajaxurl , { action: "leave_tournament", tid: settingsTournaments.data_pid }, function (data) {
        	NotifyMe(settingsTournaments.tournament_left, "error");
            location.reload();
        });
    });

    jQuery('.rejects').on('click', function(e) {
        var loader = settingsTournaments.loader;
        var ub = jQuery(this).closest('li');
        var uid = ub.data('team_id');
        ub.html('<img src='+loader+' />');
        NotifyMe(settingsTournaments.rejected, "information");
        jQuery.post( ajaxurl , { action: "rejects_user", tid: settingsTournaments.data_pid, uid: uid }, function (data) {
            ub.remove();
        });
    });

     jQuery('.restart_tournament').on('click', function(e) {
        e.preventDefault();
        NotifyMe(settingsTournaments.restarting, "information");
        jQuery.post( ajaxurl , { action: "arcane_ajax_restart_tournament", data: settingsTournaments.new_data}, function (data) {

          var tempdata = data.split('-_-');
          if (tempdata[0] == "ok") {
                window.location.href =  tempdata[1];
            }
        });
    });
	if(settingsTournaments.tour_contestants == 'user'){
		jQuery('#join_tournament_single').one('click', function(e) {
	        var loader = settingsTournaments.loader;
	        jQuery(this).html('');
	        jQuery(this).html('<img src='+loader+' />');
	        jQuery.post( ajaxurl , { action: "join_tournament", tid: settingsTournaments.data_pid, pid: settingsTournaments.current_user_id }, function (data) {
	        	NotifyMe(settingsTournaments.tournament_joined, "success");
	            location.reload();
        });
    });
	}else if((settingsTournaments.count_teams == 1) && (settingsTournaments.found_id !== false) && (settingsTournaments.inarray)){
	    jQuery('.thdescriptionleft .jtournamentb').one('click', function(e) {
	        e.preventDefault();
	        NotifyMe(settingsTournaments.joining_tournament, "information");
	        jQuery(this).html('');
        	jQuery(this).html('<img src='+settingsTournaments.loader+' />');
	        jQuery.post( ajaxurl , { action: "join_tournament", tid: settingsTournaments.data_pid, pid: settingsTournaments.teams_array }, function (data) {
	           location.reload();
	        });
	    });
	}else{
		jQuery(".thdescriptionleft .jtournamentb").one('click', function(e) {
	        arcane_filter_teams(jQuery(this).data("gid"));
	    });

	}

    jQuery('.u_confirm').one('click', function(e) {

        var loader = settingsTournaments.loader;
        var ub = jQuery(this).closest('li');
        var ubin = ub.clone();
        var uid = ub.data('team_id');
        var con = jQuery(this).data('con');
        var can = jQuery('.candidate_listing');
        jQuery(this).closest('li').addClass('acc_loader');
        ub.html('<img src='+loader+' />');

        jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            dataType:"json",
            data: {
                action: "confirm_user",
                tid: settingsTournaments.data_pid,
                uid: uid,
                con: con
            },
            success: function(data) {
                if(data == "accepted"){
                    NotifyMe(settingsTournaments.user_accepted, "information");
                    ubin.appendTo(jQuery(".competitor-list")).removeClass('candidate_listing').addClass('competitor_listing').find('.confirmation').remove();
                }else if(data == "rejected"){
                    NotifyMe(settingsTournaments.user_rejected, "information");
                }
                can.removeClass('acc_loader');
                ub.remove();
                if(jQuery('.candidate_listing').length == 0) {
                    location.reload();
                }
            }
        });

    });



    jQuery('.u_kick').one('click', function(e) {

        var loader = settingsTournaments.loader;
        var ub = jQuery(this).closest('li');
        var uid = ub.data('team_id');
        var con = jQuery(this).data('con');
        ub.html('<img class="kick_load" src='+loader+' />');

        jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            dataType:"json",
            data: {
                action: "kick_user",
                tid: settingsTournaments.data_pid,
                uid: uid,
                con: con
            },
            success: function(data) {
                if(data == "accepted"){
                    NotifyMe(settingsTournaments.user_kicked, "information");
                }
                ub.remove();
                location.reload();
            }
        });

    });



    var options = {pk: settingsTournaments.data_pid, url: ajaxurl, mode: 'inline'};
    var counter = 0;
   var Regulations = parseInt(settingsTournaments.regulations_num);
    while(counter < Regulations){
    	jQuery('#regulations_tab_name_'+counter).editable(options);
        jQuery('#regulations_tab_content_'+counter).editable(options);

        counter++;
    }


    jQuery("#publish_tournament").on('click', function(e) {
        ArcaneRemoveCogs();
        var KeepHtml = jQuery(this).html();
        var loader = settingsTournaments.loader;
        jQuery(this).html('');
        jQuery(this).html('<img src='+loader+' />');
        var errored = false;
        var data = {action:"publish"};
        jQuery('.editable').each(function(e) {
            switch (jQuery(this).attr('id')) {
                case 'tournament_title':
                    data.tournament_title = jQuery(this).text();
                    var eltext = jQuery(this).text();
                        if (eltext.trim() == settingsTournaments.demo_data_title) {
                            errored = true;
                            NotifyMe(settingsTournaments.empty_name, "error");
                        }
                    break;
                case 'tournament_starts':
                    data.tournament_starts = jQuery(this).text();
                    data.tournament_starts_unix = jQuery(this).attr("data-unix");
                    break;
                case 'tournament_description':
                    data.tournament_description = jQuery(this).html();
                    var eltext = jQuery(this).text();
                        if (eltext.trim() == settingsTournaments.tournament_desc) {
                            errored = true;
                            NotifyMe(settingsTournaments.tournament_desc_empty, "error");
                        }
                    break;
                case 'tournament_max_participants':
                    data.tournament_max_participants = jQuery(this).text();
                    var eltext = jQuery(this).text();
                        if (eltext == settingsTournaments.max_participants) {
                            errored = true;
                            NotifyMe(settingsTournaments.participants_empty, "error");
                        }
                    break;
                case 'tournament_format':
                    data.tournament_format = jQuery(this).attr("data-format" );
                    var eltext = jQuery(this).attr("data-format" );
                        if (typeof eltext !== 'undefined' && eltext.trim() == settingsTournaments.tournament_format) {
                            errored = true;
                            NotifyMe(settingsTournaments.tournament_empty, "error");
                        }
                    break;
                case 'tournament_timezone':
                    data.tournament_timezone = jQuery(this).text();
                    var eltext = jQuery(this).text();
                    break;
                case 'tournament_server':
                    data.tournament_server = jQuery(this).text();
                    var eltext = jQuery(this).text();
                        if (eltext.trim() == settingsTournaments.tournament_server) {
                            errored = true;
                            NotifyMe(settingsTournaments.empty_server, "error");
                        }
                    break;
                case 'tournament_platform':
                    data.tournament_platform = jQuery(this).text();
                    break;
                case 'game_name':
                    data.game_name = jQuery(this).text();
                    var eltext = jQuery(this).text();
                        if (eltext.trim() == settingsTournaments.tournament_game) {
                            errored = true;
                            NotifyMe(settingsTournaments.game_empty, "error");
                        }
                    break;
                case 'tournament_contestants':
                    data.tournament_contestants = jQuery(this).attr("data-contestants" );
                    var eltext = jQuery(this).attr("data-contestants" );

                        if (typeof eltext !== 'undefined' && eltext.trim() == settingsTournaments.tournament_contestans) {
                            errored = true;
                            NotifyMe(settingsTournaments.contestants_empty, "error");
                        }
                    break;
                case 'tournament_games_format':
                    data.tournament_games_format = jQuery(this).text();
                    var eltext = jQuery(this).text();
                        if (eltext.trim() == settingsTournaments.games_format) {
                            errored = true;
                            NotifyMe(settingsTournaments.game_format_empty, "error");
                        }
                    break;
                case 'tournament_game_frequency':

                    let format = jQuery('#tournament_format').attr('data-format');
                    if(format === 'ladder') break;

                    data.tournament_game_frequency = jQuery(this).text();
                    var eltext = jQuery(this).text();
                        if (eltext.trim() == settingsTournaments.game_frequency) {
                            errored = true;
                            NotifyMe(settingsTournaments.game_frequency_empty, "error");
                        }
                    break;
            }

        });
        //now check if we added prizes
        var prizes = jQuery(".prize_text").size();
        if (prizes == 0) {
            NotifyMe(settingsTournaments.set_prizes, "information");
        } else {
            var temparray = [];
            jQuery(".prize_text").each(function(e) {
                var holder = jQuery(this).attr('id');
                holder = holder.replace('tournament_prize_', '');
                temparray[holder] = jQuery(this).text();

            });
            data.prizes = temparray;
        }

        data.tournament_frequency = jQuery('#tournament_frequency').text();

        data.game_modes = jQuery('#game_modes').text();

        //and regulations
        var prizes = jQuery('a[id^="regulations_tab_a_"]').size();
        if (prizes == 0) {
            NotifyMe(settingsTournaments.set_rules, "information");
        } else {
            var temphold = [];
            jQuery('a[id^="regulations_tab_a_"]').each(function(e) {
                var tempid = jQuery(this).attr('id');
                tempid = tempid.replace('regulations_tab_a_', '');
                var tempname = jQuery("#regulations_tab_name_"+ tempid).text();
                var tempcontent = jQuery("#regulations_tab_content_"+ tempid).html();
                var tempobject = {'name': tempname, 'content':tempcontent};
                temphold[tempid] = tempobject;
            });
            data.regulations = temphold;
        }
        var tempmaps = [];
        jQuery('.selected_map').each(function(e) {
            tempmaps.push(jQuery(this).data('map-id'));
        });
        data.maps = tempmaps;

        if (errored == true) {
            jQuery(this).html('');
            jQuery(this).html(KeepHtml);
        } else {
            NotifyMe(settingsTournaments.publishing_tournament, "success");

            jQuery.post( ajaxurl , { action: "arcane_update_tournament", pk: settingsTournaments.data_pid, alldata: data }, function (dataz) {
            	console.log(dataz);
                var tempdata = dataz.split('-_-');
                if (tempdata[0] == "ok") {
                    window.location.href =  tempdata[1];
                }
            });
        }

    });

    jQuery("#saveasdraft_tournament").on('click', function(e) {
        ArcaneRemoveCogs();
        var errored = false;
        var data = {action:"draft"};
        var KeepHtml = jQuery(this).html();
        var loader = settingsTournaments.loader;
        jQuery(this).html('');
        jQuery(this).html('<img src='+loader+' />');
        jQuery('.editable').each(function(e) {
            switch (jQuery(this).attr('id')) {
                case 'tournament_title':
                    data.tournament_title = jQuery(this).text();
                    break;
                case 'tournament_starts':
                    data.tournament_starts = jQuery(this).text();
                    data.tournament_starts_unix = jQuery(this).attr("data-unix");
                    break;
                case 'tournament_timezone':
                    data.tournament_timezone = jQuery(this).text();
                    break;
                case 'tournament_description':
                    data.tournament_description = jQuery(this).html();
                    break;
                case 'tournament_max_participants':
                    data.tournament_max_participants = jQuery(this).text();
                    break;
                case 'tournament_format':
                    data.tournament_format = jQuery(this).data( "format" );
                    break;
                case 'tournament_server':
                    data.tournament_server = jQuery(this).text();
                    break;
                case 'tournament_platform':
                    data.tournament_platform = jQuery(this).text();
                    break;
                case 'game_name':
                    data.game_name = jQuery(this).text();
                    break;
                case 'tournament_contestants':

                    data.tournament_contestants = jQuery(this).data( "contestants" );
                    break;
                case 'tournament_games_format':
                    data.tournament_games_format = jQuery(this).text();
                    break;
                case 'tournament_game_frequency':
                    data.tournament_game_frequency = jQuery(this).text();
                    break;
                case 'tournament_frequency':
                    data.tournament_frequency = jQuery(this).text();
                    break;
                case 'game_modes':
                    data.game_modes = jQuery(this).text();
                    break;
            }
        });
        //now check if we added prizes
        var prizes = jQuery(".prize_text").size();
        if (prizes == 0) {

        } else {
            var temparray = [];
            jQuery(".prize_text").each(function(e) {
                var holder = jQuery(this).attr('id');
                holder = holder.replace('tournament_prize_', '');
                temparray[holder] = jQuery(this).text();
            });
            data.prizes = temparray;
        }

        var tempmaps = [];
        jQuery('.selected_map').each(function(e) {
            tempmaps.push(jQuery(this).data('map-id'));
        });
        data.maps = tempmaps;

        //and regulations
        var prizes = jQuery('a[id^="regulations_tab_a_"]').size();
        if (prizes == 0) {
        } else {
            var temphold = [];
            jQuery('a[id^="regulations_tab_a_"]').each(function(e) {
                var tempid = jQuery(this).attr('id');
                tempid = tempid.replace('regulations_tab_a_', '');
                var tempname = jQuery("#regulations_tab_name_"+ tempid).text();
                var tempcontent = jQuery("#regulations_tab_content_"+ tempid).html();
                var tempobject = {'name': tempname, 'content':tempcontent};
                temphold[tempid] = tempobject;
            });
            data.regulations = temphold;
        }
//          if (TimeMomentObject != false) {
//              data.TimeObject = TimeMomentObject;
//          }
            NotifyMe(settingsTournaments.saving_draft, "success");
            jQuery.post( ajaxurl , { action: "arcane_update_tournament", pk: settingsTournaments.data_pid, alldata: data }, function (dataz) {
                var tempdata = dataz.split('-_-');
                if (tempdata[0] == "ok") {
                    window.location.href =  tempdata[1];
                }
            });


    });



    jQuery("#delete_tournament").on('click', function(e) {
        bootbox.confirm(settingsTournaments.you_want_delete, function(result) {
            if (result == true) {
                NotifyMe(settingsTournaments.deleting_tournament, "error");
                jQuery.post( ajaxurl , { action: "delete_tournament", tid: settingsTournaments.data_pid }, function (data) {
                    window.location.href = settingsTournaments.redirect;
                });
            }
        });
    });



    var remove_premium = jQuery("#remove_premium");
    var make_premium = jQuery("#make_premium");

    make_premium.on("click", function(event){
       var make_premium = jQuery("#make_premium");
       var c_panel = jQuery(".tournament_control_panel");
       var thdescriptionleft = jQuery('.thdescriptionleft');
      jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            dataType:"json",
            data: {"action": "arcane_add_to_premium", 'security' : settingsGlobal.security, tid: settingsTournaments.data_pid },
            success: function(data) {
            NotifyMe(settingsTournaments.tournament_premium, "information");
            make_premium.remove();
            c_panel.prepend("<a id='remove_premium' class='button-small'>"+settingsTournaments.remove_premium+"</a>");
            thdescriptionleft.prepend("<div class='premium-tag'><i class='fa fa-money' aria-hidden='true'></i> "+settingsTournaments.t_premium+"</div>");
            return false;
            }
        });
    });


    remove_premium.on("click", function(event){
        var remove_premium = jQuery("#remove_premium");
        var c_panel = jQuery(".tournament_control_panel");
        var premium_tag = jQuery(".premium-tag");
      jQuery.ajax({
            type: "POST",
            url: ajaxurl,
            dataType:"json",
            data: {"action": "arcane_remove_premium", 'security' : settingsGlobal.security, tid: settingsTournaments.data_pid },
            success: function(data) {
            NotifyMe(settingsTournaments.removed_premium, "information");
            remove_premium.remove();
            premium_tag.remove();
            c_panel.prepend("<a id='make_premium' class='button-small'>"+settingsTournaments.make_premium+"</a>");
            return false;
            }
        });
    });



	jQuery("#edit_tournament").on('click', function(e) {
		e.preventDefault();
		var serializedData="";
		jQuery.post( ajaxurl , { action: "edit_a_tournament", data:  jQuery(".wrap form").serialize() }, function (data) {});
	});


	jQuery("#submitdiv").slideUp(function(e){
		//replace the submitdiv with something else entirely
		jQuery("#submitdiv .inside").html(settingsTournaments.try_t);
		jQuery("#submitdiv").slideDown();
	});


});

function ArcaneRemoveCogs() {
    jQuery('.fa-cog').remove();
}
