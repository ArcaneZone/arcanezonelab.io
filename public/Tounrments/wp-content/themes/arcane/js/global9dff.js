function shopping_cart_dropdown() {
    'use strict';
    !jQuery(".widget_shopping_cart .widget_shopping_cart_content .cart_list .empty").length && jQuery(".widget_shopping_cart .widget_shopping_cart_content .cart_list").length > 0 && jQuery(".cart-menu-wrap").addClass("has_products");
}
//----Unique team name start-----
function GetURLParameter(sParam) {
    //var sPageURL = window.location.search.substring(1);
    'use strict';
    var sPageURL = (window.location !== window.parent.location)
            ? document.referrer
            : document.location + '';
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}


//----Unique team name end-----
function shopping_cart_dropdown_show(t) {
    clearTimeout(timeout), !jQuery(".widget_shopping_cart .widget_shopping_cart_content .cart_list .empty").length && jQuery(".widget_shopping_cart .widget_shopping_cart_content .cart_list").length > 0 && "undefined" != typeof t.type && (jQuery(".container .cart-menu-wrap").hasClass("has_products") ? jQuery(".container .cart-notification").is(":visible") ? jQuery(".container .cart-notification").show() : jQuery(".container .cart-notification").fadeIn(400) : setTimeout(function() {
        jQuery(".container .cart-notification").fadeIn(400)
    }, 400), timeout = setTimeout(hideCart, 2700))
}

function hideCart() {
    jQuery(".container .cart-notification").stop(!0, !0).fadeOut()
}

function NotifyMe(t, e, sticky) {
    "use strict";
    var temptime = 5e3;
    if (sticky == true) {
      temptime = false;
    }

    noty({
        text: t,
        type: e,
        animation: {
            open: "animated fadeInUp",
            close: "animated fadeOutDown",
            easing: "swing",
            speed: 500
        },
        dismissQueue: !0,
        maxVisible: 10,
        timeout: temptime,
        layout: "topLeft"
    })
}
//----Unique team name start-----
var FoundElements = false;
var Changed = false;
function CheckTeamName() {
    //code

    jQuery("#vc_page-title-field", window.parent.document).on('keydown', function(e) {
        if (FoundElements == true) {
            jQuery('*[data-vc-ui-element="button-save"]', window.parent.document).css('display', 'none');
            jQuery('*[data-vc-ui-element="button-close"]', window.parent.document).on('click', function(e) {
                jQuery('*[data-vc-ui-element="button-save"]', window.parent.document).css('display', 'inline-block');
            });
            Changed = true;
        }

    });
    jQuery("#vc_page-title-field", window.parent.document).on('focusout', function(e) {
        if ((FoundElements == true) && (Changed == true)) {
            //you can add checking stuff here
            if (!jQuery("#status_checker", window.parent.document).length ) {
                //status field isn't there
                jQuery("#vc_page-title-field", window.parent.document).after('<div id="status_checker"></div>');
            }
            if (jQuery("#status_checker", window.parent.document).is(":visible")){

                jQuery("#status_checker", window.parent.document).fadeOut("slow", function() {
                    jQuery("#status_checker", window.parent.document).html(settingsGlobal.checking_name);
                    jQuery("#status_checker", window.parent.document).fadeIn("slow");
                });
            } else {

                 jQuery("#status_checker", window.parent.document).html(settingsGlobal.checking_name);
                jQuery("#status_checker", window.parent.document).fadeIn("slow");
            }

            //like animation or text
            jQuery.post( ajaxurl , { action: "arcane_check_if_teamname_unique",'security' : settingsGlobal.security, currentText: jQuery("#vc_page-title-field", window.parent.document).val() }, function (data) {
               //console.log(data);
                if (data == "/*-notunique*-/") {
                    //is NOT unique disallow saving
                    jQuery("#status_checker", window.parent.document).fadeOut("slow", function() {
                        jQuery("#status_checker", window.parent.document).html(settingsGlobal.taken_name);
                        jQuery("#status_checker", window.parent.document).fadeIn("slow");
                    });


                } else {

                    //we're just fine
                     jQuery("#status_checker", window.parent.document).fadeOut("slow", function() {
                        jQuery("#status_checker", window.parent.document).html(settingsGlobal.available_name);
                        jQuery("#status_checker", window.parent.document).fadeIn("slow");
                    });
                    jQuery("#vc_page-title-field", window.parent.document).val(data);
                    jQuery('*[data-vc-ui-element="button-save"]', window.parent.document).fadeIn();
                }
            });
            Changed = false;
        }
    });
    if (jQuery("#vc_page-title-field", window.parent.document).val() != undefined) {
        //do nada
    } else {
        //try again a bit later
        setTimeout(CheckTeamName, 300);
    }

}


 jQuery(document).ready(function() {
    "use strict";
    var t = jQuery("ul.sub-menu");
    t.parent().addClass("dropdown"), t.addClass("dropdown-menu");
    var e = jQuery("dropdown-menu.children");
    e.parent().addClass("dropdown"), e.addClass("dropdown-menu");
    var r = jQuery("dropdown-menu.children");
    r.parent().addClass("dropdown");
    var o = jQuery(".menu ul");
    o.parent().addClass("dropdown");
    var i = jQuery(".dropdown .dropdown-menu");
    i.removeClass("children");
    var n = jQuery(".dropdown > a br");
    n.before('<b class="caret"></b>'), r.hover(function() {
        jQuery(this).parent().addClass("active")
    }, function() {
        jQuery(this).parent().removeClass("active")
    }), t.hover(function() {
        jQuery(this).parent().addClass("active")
    }, function() {
        jQuery(this).parent().removeClass("active")
    });
    var a = jQuery(".menu ul");


    if (GetURLParameter('post_type') == "team") {
    	CheckTeamName();
        jQuery('.pmi_title').on('click', function(e) {
            FoundElements = true;
        });
    }
    a.addClass("nav"), e.removeClass("nav")
}),

//----Unique team name end-----

jQuery.noConflict(), jQuery(document).ready(function() {
    "use strict";
    var t = jQuery("#searchform #searchsubmit");
    t.remove();
    var c = jQuery("#searchform input[type='text']");
    c.remove();
    var e = jQuery("#searchform .screen-reader-text");
    e.remove();
    var r = jQuery("#searchform div");
    r.append('<input type="hidden" placeholder="'+settingsGlobal.searchFor+'" value="post" name="post_type">');
    var r = jQuery("#searchform div");
    r.append('<input type="text" placeholder="'+settingsGlobal.searchFor+'" name="s" id="s">');
    var o = jQuery("#searchform div h3");
    o.remove()
}),


jQuery.noConflict(), jQuery(function() {
    "use strict";
    jQuery("[data-toggle=tooltip]").tooltip()
}),




jQuery.noConflict();
var wgtitle = jQuery(".widget");
wgtitle.each(function() {
    "use strict";
    var t = jQuery(this).find('.bbp-forum-title');
    t.prepend(" ").prepend('<i class="far fa-comments" aria-hidden="true"></i>');
});



var timeout, productToAdd;
jQuery(".product-wrap .add_to_cart_button").on('click', function(e) {
    productToAdd = jQuery(this).parents("li").find("h3").text(), jQuery(".container .cart-notification span.item-name").html(productToAdd)
}), jQuery(".container .cart-notification").hover(function() {
    jQuery(this).fadeOut(400), jQuery(".container .widget_shopping_cart").stop(!0, !0).fadeIn(400), jQuery(".container .cart_list").stop(!0, !0).fadeIn(400), clearTimeout(timeout)
}), jQuery(".container div.cart-outer").hover(function() {
    jQuery(".container .widget_shopping_cart").stop(!0, !0).fadeIn(400), jQuery(".container .cart_list").stop(!0, !0).fadeIn(400), clearTimeout(timeout), jQuery(".container .cart-notification").fadeOut(300)
}, function() {
    jQuery(".container .widget_shopping_cart").stop(!0, !0).fadeOut(300), jQuery(".container .cart_list").stop(!0, !0).fadeOut(300)
}), jQuery("body").bind("added_to_cart", shopping_cart_dropdown_show), jQuery("body").bind("added_to_cart", shopping_cart_dropdown), setTimeout(shopping_cart_dropdown, 550);




var mcontainer = jQuery(".mcontainer script");
mcontainer.unwrap();



var bbip = jQuery(".bbp-author-ip");
bbip.remove();


var ticker = jQuery("#webticker");
ticker.webTicker({
	speed: settingsGlobal.tickerspeed
});



jQuery(document).ready(function() {
    "use strict";
    var t = jQuery(".login-tooltip"),
        e = jQuery(".login-btn"),
        r = jQuery("#login_tooltip .closeto"),
        o = jQuery(".navbar-wrapper");
    e.on('click', function(e) {
        t.fadeTo("fast", 1, function() {
            jQuery(this).css("top", "50%"), o.css("z-index", "1000")
        })
    }), r.on("click", function() {
        t.fadeTo("fast", 0, function() {
            jQuery(this).css("top", "-5000px"), o.css("z-index", "99999999")
        })
    })
}),


jQuery(document).ready(function() {
    "use strict";
    var t = jQuery(".tab-inner");
    if (0 !== t.length) {
        var e = jQuery('.tab-inner a[href="#tab-1"]').parent().index();
        t.tabs().tabs("option", "active", e)
    }

    var c = jQuery(".the_champ_vertical_sharing");
    var mw = jQuery("#main_wrapper");
    if (0 !== c.length) {
   		mw.prepend(c);
   	}
}),






jQuery(document).ready(function() {
    "use strict";
    jQuery("a.back-to-top").on('click', function(e) {
        jQuery("html, body").animate({
            scrollTop: 0
        }, 800)
    })
}),



jQuery(document).ready(function() {
    "use strict";
    var t = jQuery("#members-dir-search"),
        e = jQuery("#subnav");
    e.before(t)
}),


jQuery(document).ready(function(t) {
    "use strict";
    var e = t("#contactForm");
    e.validate();

    var online = jQuery('.footer_widget .widget-error');
    online.addClass('avatar-block').removeClass('widget-error');

    jQuery('.limit_to_numbers').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    jQuery('.limit_to_url').focusout(function() {
    	var container = jQuery(this).val();
    	if (container.length > 0) {
    		if (!isUrlValid(container)) {
    			jQuery(this).css('border-color', '#ff0000');

	    	} else {
	    		jQuery(this).css('border-color', '');
	    	}
    	}
    });

});



function isUrlValid(url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}

jQuery(document).ready(function() {
var edit_image = jQuery('.edit-attachment');
var samaslika = jQuery('.js--select-attachment');
samaslika.on( "click", function() {
edit_image.remove();
});

})



/******************** Isotope blog ***********************/
 var Waiting = false;
var blog = jQuery(".page-template-tmp-blog-isotope");
if(blog.length != 0){

// Modified Isotope methods for gutters in masonry

jQuery.Isotope.prototype._getMasonryGutterColumns = function() {
	var gutter = this.options.masonry && this.options.masonry.gutterWidth || 0;
	var containerWidth = this.element.width();

	this.masonry.columnWidth = this.options.masonry && this.options.masonry.columnWidth ||
	// Or use the size of the first item
	this.jQueryfilteredAtoms.outerWidth(true) ||
	// If there's no items, use size of container
	containerWidth;

	this.masonry.columnWidth += gutter;

	this.masonry.cols = Math.floor((containerWidth + gutter) / this.masonry.columnWidth);
	this.masonry.cols = Math.max(this.masonry.cols, 1);
};

jQuery.Isotope.prototype._masonryReset = function() {
	// Layout-specific props
	this.masonry = {};
	// FIXME shouldn't have to call this again
	this._getMasonryGutterColumns();
	var i = this.masonry.cols;
	this.masonry.colYs = [];
	while (i--) {
		this.masonry.colYs.push(0);
	}
};

jQuery.Isotope.prototype._masonryResizeChanged = function() {
	var prevSegments = this.masonry.cols;
	// Update cols/rows
	this._getMasonryGutterColumns();
	// Return if updated cols/rows is not equal to previous
	return (this.masonry.cols !== prevSegments);
};
// modified Isotope methods for gutters in masonry
if(jQuery.isFunction(jQuery.fn.imagesLoaded)){

	//isotope
	var containerblog = jQuery('.isoblog');

	containerblog.imagesLoaded( function(){
	// initialize Isotope

	containerblog.isotope({
		// options...
		layoutMode : 'masonry',
		resizable: false, // disable normal resizing
		// set columnWidth to a percentage of container width
		masonry: {
			columnWidth: (containerblog.width() - 40) / 4,
			gutterWidth: 20
		}
	});
	});

	// start new block
	jQuery('.cat a').on('click', function(e) {
		var selector = jQuery(this).attr('href');
		containerblog.isotope({ filter: selector });
		return false;
	});
	// end new block

	// update columnWidth on window resize
	jQuery(window).smartresize(function(){
		//console.log(container.width());
		// set the widths on resize
		setWidthsBlog();
		containerblog.isotope({
			// update columnWidth to a percentage of container width
			masonry: {
				columnWidth: getUnitWidthBlog(),
				gutterWidth: 20
			}
		});
	}).resize();
}


/*  Isotope utility SetWidths
    ========================================================================== */
function setWidthsBlog() {
	var unitWidth = getUnitWidthBlog() - 0;
	containerblog.children(":not(.width2)").css({
		width: unitWidth
	});

	if (containerblog.width() >= 321 && containerblog.width() <= 480) {
		//console.log("eccoci 321");
		containerblog.children(".width2").css({
			width: unitWidth * 1
		});
		containerblog.children(".width4").css({
			width: unitWidth * 2
		});
		containerblog.children(".width6").css({
			width: unitWidth * 3
		});
	}
	if (containerblog.width() >= 481) {
		//console.log("480");
		containerblog.children(".width6").css({
			width: unitWidth * 4
		});
		containerblog.children(".width4").css({
			width: unitWidth * 3
		});
		containerblog.children(".width2").css({
			width: unitWidth * 2
		});
	} else {
		containerblog.children(".width2").css({
			width: unitWidth
		});
	}
}

/*  Isotope utility GetUnitWidth
    ========================================================================== */
function getUnitWidthBlog() {
	var width;
	if (containerblog.width() <= 320) {
		//console.log("320");
		width = Math.floor((containerblog.width() - 20)  / 1);
	} else if (containerblog.width() >= 321 && containerblog.width() <= 480) {
		//console.log("321 - 480");
		width = Math.floor((containerblog.width() - 20)  / 1);
	} else if (containerblog.width() >= 481 && containerblog.width() <= 662) {
		//console.log("481 - 768");
		width = Math.floor((containerblog.width() - 20) / 1);
	} else if (containerblog.width() >= 663 && containerblog.width() <= 768) {
		//console.log("663 - 768");
		width = Math.floor((containerblog.width() - 20)  / 2);
	} else if (containerblog.width() >= 769 && containerblog.width() <= 979) {
		//console.log("769 - 979");
		width = Math.floor((containerblog.width() - 40)  / 3);
	} else if (containerblog.width() >= 980 && containerblog.width() <= 1200) {
		//console.log("980 - 1200");
		width = Math.floor((containerblog.width() - 40)  / 4);
	} else if (containerblog.width() >= 1201 && containerblog.width() <= 1600) {
		//console.log("1201 - 1600");
		width = Math.floor((containerblog.width() - 60)  / 4);
	} else if (containerblog.width() >= 1601 && containerblog.width() <= 1824) {
		//console.log("1601 - 1824");
		width = Math.floor((containerblog.width() - 20)  / 8);
	} else if (containerblog.width() >= 1825) {
		//console.log("1825");
		width = Math.floor((containerblog.width() - 20)  / 10);
	}
	return width;
}


jQuery(document).ready(function() {
	   	if(Waiting == false) {
	    setTimeout(Resizer, 800);
	    Waiting = true;
	}
	 jQuery( window ).resize(function() {
	             if (Waiting == false) {

                        setTimeout(Resizer, 400);
                        Waiting = true;
                    }
                });

    function Resizer() {
	if(jQuery.isFunction(jQuery.fn.imagesLoaded)){
		var container = jQuery('.isoprblck');
		if (container) {
	        jQuery(window).smartresize(function(){
			//console.log(container.width());
			// set the widths on resize
			setWidthsBlog();
			container.isotope({
				// update columnWidth to a percentage of container width
				masonry: {
					columnWidth: getUnitWidthBlog()
				}
			});
		}).resize();
		}
	    Waiting = true;
    }
}

});
}



/******************** Search form ***********************/
jQuery(document).ready(function(t) {
	var src_input = jQuery("#sform input[type=search]");
	var src_all = jQuery("#sform input[type=search], #sform .fas");

	jQuery(document).on('click', function(e) {
	src_input.css('width', '3px');
	src_input.css('padding', '9px 18px 8px 18px');
	src_input.css('border', '0px solid transparent');
	src_input.css('cursor', 'pointer');
	src_input.css('background-color', '');
	src_input.val('');
	});

	src_all.on('click', function(e) {
	e.stopPropagation();
	src_input.css('width', '130px');
	src_input.css('color', '#000');
	src_input.css('padding-left', '32px');
	src_input.css('border', '0px');
	src_input.css('cursor', 'auto');
	src_input.css('background-color', '#202126');
	});


});

/***************tournament page accordion***************/
	jQuery('#show_more').on('click', function(e) {
		var $collapse = jQuery('#collapseOne'),
		$this =jQuery(this);

		if($collapse.hasClass('in')){
			$this.text(settingsGlobal.tournaments_more);
		} else {
			$this.text(settingsGlobal.tournaments_hide);
		}
	});


	/*all teams for selected game*/
	jQuery( document ).ready(function($) {
	 	if(settingsGlobal.game_page == 'yes'){
			var $members_search_submit = jQuery('#members_search_submit');
			$members_search_submit.attr('id', 'members_search_submit_games');

			var $members = jQuery('#members');
			$members.attr('id', 'members_games');
		}

	 	jQuery('#members_search_submit_games').on('click', function(event){
	        event.preventDefault();
	        jQuery('#members_games').load(ajaxurl, {
	            "action":"arcane_list_all_teams_for_selected_game_ajax",
				"href": '&page=1&term='+jQuery('#team_name').val(),
                'security' : settingsGlobal.security
	            });
	    });


	    jQuery('#members_games').on('click', '.pagination a', function(event){
	        event.preventDefault();
	        jQuery('#members_games').load(ajaxurl, {
	            "action":"arcane_list_all_teams_for_selected_game_ajax",
	            "href":jQuery(this).attr('href'),
                'security' : settingsGlobal.security
	            });
	    });

	});


 	jQuery('#members_search_submit').on('click', function(event){
        event.preventDefault();
        jQuery('#members').load(ajaxurl, {
            "action":"arcane_all_teams_pagination_v2_ajax",
			"href": '?page=1&term='+jQuery('#team_name').val(),
            'security' : settingsGlobal.security
            });
    });

    jQuery('#members').on('click', '.pagination a', function(event){
        event.preventDefault();
        jQuery('#members').load(ajaxurl, {
            "action":"arcane_all_teams_pagination_v2_ajax",
            "href":jQuery(this).attr('href'),
            'security' : settingsGlobal.security
            });
    });

    jQuery('#tournaments_search_submit').on('click', function(event){
        event.preventDefault();
        jQuery('#members').load(ajaxurl, {
            "action":"arcane_all_tournaments_pagination_v2_ajax",
			"href": '?page=1&term='+jQuery('#tournament_name').val(),
            'security' : settingsGlobal.security
            });
    });

    jQuery('#tournament_members').on('click', '.pagination a', function(event){
        event.preventDefault();
        jQuery('#tournament_members').load(ajaxurl, {
            "action":"arcane_all_tournaments_pagination_v2_ajax",
            "href":jQuery(this).attr('href'),
            'security' : settingsGlobal.security
            });
    });



/*change profile image in single team page*/
 jQuery( document ).ready(function($) {

		/*TEAM PAGE PHOTOS*/
		 if (settingsGlobal.admin == true) { var admin =  1; } else {var admin =  0;}
		 if (settingsGlobal.mine_team == true) { var mine_team =  1; } else {var mine_team =  0;}
		if(admin == 1 || mine_team == 1){
        var _custom_media = true, _orig_send_attachment = wp.media.editor.send.attachment;
        //profile-media-team
        jQuery('.team-avatar-card').mouseenter(function(e) {
            jQuery('#change_profile_pic').fadeTo('slow', 0.75);
        });
        jQuery('.team-avatar-card').mouseleave(function(e) {
            jQuery('#change_profile_pic').fadeOut();
        });

        jQuery(".team-avatar-card").on( "click", function() {
            DoChangeProfile(settingsGlobal.p_id);
        });

        jQuery('.photo').css("cursor", "pointer");
        var style = [{'cursor':'pointer','opacity':'1','display':'block'}];
        jQuery(".team-avatar-card img").css(style[0]);
        jQuery('#change_profile_pic').css("cursor", "pointer");
        function DoChangeProfile(pid) {
            var send_attachment_bkp = wp.media.editor.send.attachment;
            wp.media.editor.send.attachment = function(props, attachment) {
                jQuery(".team-avatar-card img").fadeOut('slow', function(e) {
                    jQuery(".team-avatar-card img").attr('src', attachment.url);
                    jQuery(".team-avatar-card img").on('load', function(){
                        jQuery(".team-avatar-card img").fadeIn();
                        var style = [{'cursor':'pointer','opacity':'1','display':'block'}];
                        jQuery(".team-avatar-card img").css(style[0]);
                    });
                    var data = {
                        'action': 'update_team_pic',
                        'file': attachment.url,
                        'security' : settingsGlobal.security,
                        idp: pid
                    };
                    $.post(ajaxurl, data, function(response) {
                       NotifyMe(response, "information");
                    });
                });

                wp.media.editor.send.attachment = send_attachment_bkp;
            }
            wp.media.editor.open();
            return false;
        }




         //profile-media-team
        jQuery('.profile-media-team').mouseenter(function(e) {
            jQuery('#change_bg_pic').fadeTo('slow', 0.75);
        });
        jQuery('.profile-media-team').mouseleave(function(e) {
            jQuery('#change_bg_pic').fadeOut();
        });
        jQuery('.profile-media-team').on( "click", function() {
            DoChangeBg(settingsGlobal.p_id);
        });
        jQuery('#change_bg_pic').on( "click", function() {
            DoChangeBg(settingsGlobal.p_id);
        });
        jQuery('.profile-media-team').css("cursor", "pointer");
        jQuery(".profile-media-team img").css("cursor", "pointer");
        jQuery('#change_bg_pic').css("cursor", "pointer");
        function DoChangeBg(pid) {
            var send_attachment_bkp = wp.media.editor.send.attachment;
            wp.media.editor.send.attachment = function(props, attachment) {
                jQuery(".profile-media-team img").fadeOut('fast', function(e) {
                    jQuery(".profile-media-team img").attr('src', attachment.url);
                    jQuery(".profile-media-team img").on('load', function(){
                        jQuery(".profile-media-team img").fadeIn(100);
                        jQuery(".profile-media-team img").css("cursor", "pointer");
                    });
                    var data = {
                        'action': 'update_user_team_bg',
                        'file': attachment.url,
                        'security' : settingsGlobal.security,
                        idp: pid
                    };
                    $.post(ajaxurl, data, function(response) {
                        NotifyMe(response, "information");
                    });
                });
                wp.media.editor.send.attachment = send_attachment_bkp;
            }
            wp.media.editor.open();
            return false;
        }

       }

		/*MEMBER PAGE PHOTOS*/

        if (settingsGlobal.admin == true) { var admin =  1; } else {var admin =  0;}
         if (settingsGlobal.mine == true) { var mine =  1; } else {var mine =  0;}

		if(admin == 1 || mine == 1){
			jQuery('.avatar-card').mouseenter(function(e) {
		   		jQuery('#change_profile_pic').fadeTo('slow', 0.75);
		   	});
		   	jQuery('.avatar-card').mouseleave(function(e) {
		   		jQuery('#change_profile_pic').fadeOut();
		   	});
		   	jQuery('.photo').on('click', function(e) {
		   		DoChangeProfile_Member();
		   	});

		   	jQuery(".avatar-card img").on( "click", function() {
		   		DoChangeProfile_Member();
		   	});
		   	jQuery('#change_profile_pic').on( "click", function() {
		   		DoChangeProfile_Member();
		   	});
		   	jQuery('.photo').css("cursor", "pointer");
		   	jQuery(".avatar-card img").css("cursor", "pointer");
		   	jQuery('#change_profile_pic').css("cursor", "pointer");
		   	function DoChangeProfile_Member() {
				var send_attachment_bkp = wp.media.editor.send.attachment;
				wp.media.editor.send.attachment = function(props, attachment) {
					jQuery(".avatar-card img").fadeOut('slow', function(e) {
						jQuery(".avatar-card img").attr('src', attachment.url);
						jQuery(".avatar-card img").on('load', function(){
							jQuery(".avatar-card img").fadeIn();
							jQuery(".avatar-card img").css("cursor", "pointer");
						});
						var data = {
							'action': 'update_user_profile_pic',
					        'file': attachment.url,
                            'security' : settingsGlobal.security
					    };
					    $.post(ajaxurl, data, function(response) {
							NotifyMe(response, "information");
						});

					});

					if(settingsGlobal.mine == 0 && admin == 1){}else{
					jQuery(".user-avatar img").fadeOut('slow', function(e) {
						jQuery(".user-avatar img").attr('src', attachment.url);
						jQuery(".user-avatar img").on('load', function(){
							jQuery(".user-avatar img").fadeIn();
						});
					});
					}
				    wp.media.editor.send.attachment = send_attachment_bkp;
				}

				wp.media.editor.open();

				return false;
		   	}


		//profile-media
		   	jQuery('.profile-media').mouseenter(function(e) {
		   		jQuery('#change_bg_pic').fadeTo('slow', 0.75);
		   	});
		   	jQuery('.profile-media').mouseleave(function(e) {
		   		jQuery('#change_bg_pic').fadeOut();
		   	});
		   	jQuery('.profile-media').on( "click", function() {
		   		DoChangeBg_Member();
		   	});
		   	jQuery('#change_bg_pic').on( "click", function() {
		   		DoChangeBg_Member();
		   	});
		   	jQuery('.profile-media').css("cursor", "pointer");
		   	jQuery(".profile-media img").css("cursor", "pointer");
		   	jQuery('#change_bg_pic').css("cursor", "pointer");
		   	function DoChangeBg_Member() {
				var send_attachment_bkp = wp.media.editor.send.attachment;
				wp.media.editor.send.attachment = function(props, attachment) {
					jQuery(".profile-media img").fadeOut('slow', function(e) {
						jQuery(".profile-media img").attr('src', attachment.url);
						jQuery(".profile-media img").on('load', function(){
							jQuery(".profile-media img").fadeIn();
							jQuery(".profile-media img").css("cursor", "pointer");
						});
						var data = {
							'action': 'update_user_profile_bg',
					        'file': attachment.id,
                            'security' : settingsGlobal.security
					    };
					    $.post(ajaxurl, data, function(response) {
					    	NotifyMe(response, "information");
							//console.log (response);
						});

					});
				    wp.media.editor.send.attachment = send_attachment_bkp;
				}

				wp.media.editor.open();

				return false;
		   	}
		}


      /*members pagination*/
      jQuery('#buddypress').on('click', '#pag-top.pagination a, #pag-bottom.pagination a', function(event){
			event.preventDefault();
			jQuery('.single-team #buddypress').load(ajaxurl, {
				"action":"arcane_team_members_ajax",
				"page":jQuery(this).attr('href'),
				"pid": settingsGlobal.p_id,
				"uid": settingsGlobal.c_id,
                'security' : settingsGlobal.security
				});
		});


	/*notify about challenge*/
	if(settingsGlobal.challenge_sent == 'yes'){
		NotifyMe(settingsNoty.challenge_request_sent, "information");
	}


	/*change forum page title*/

	if(settingsGlobal.bb_has_forums == 'yes'){
		var forumtitle = jQuery('.bbpress .title_wrapper h1');
		forumtitle.html(settingsGlobal.newforumtitle);
	}


	/*registration successful*/

	if(settingsGlobal.registration_successful == 'yes'){
		NotifyMe(settingsGlobal.registration_successful_msg, "information");
	}



	/*sa single strane*/
	if(settingsGlobal.submitted == 'submitted'){
		NotifyMe(settingsGlobal.submitted_string, "information");
	}

	if(settingsGlobal.reported == 'reported'){
		NotifyMe(settingsGlobal.reported_string, "information");
	}

	if(settingsGlobal.already_submitted == 'submittedalready'){
		NotifyMe(settingsGlobal.already_submitted_string, "information");
	}

		if(settingsGlobal.singular_matches == 'yes'){
			var data = settingsGlobal.encoded_data;
			if(settingsGlobal.pid == 'no'){
			    data = JSON.parse(data);
			}

			if(data !== null && data !== '[]'){
		        $.each(data, function (i, item) {

		            var m = wpMatchManager.addMap(i, item.map_id);
		            for(var j = 0; j < item.team1.length; j++) {
		                m.addRound(item.team1[j], item.team2[j]);
		            };
		             var img = $('#mapsite .leftcol img');
		             var title = $('#mapsite .map .title span');
		             img.prop("src", item.map_pic);
		             title.text(item.map_title);
		        });
		    }
	   }

	/*clan challenge*/
	if(settingsGlobal.mid == 'yes'){

		 var chl = $('#wp-cw-submit');
         var title = $('#m_title');
         chl.bind('click', function(event) {
            var titlelabel = $('.form-required .tit');
            var error = $('.ermaps');


	        if(error.length ===1)error.remove();

            if(!title.val()){

                 title.after('<label for="m_title" class="ermaps tit">'+settingsGlobal.errmsg+'</label>');
                 return false;

            }

            });

	}else{

		var chl = $('#wp-cw-submit');
        var mapsite = $('#mapsite');
        var addmap = $('#wp-cw-addmap');
        var title = $('#m_title');
        var addmapbutton = $('#wp-cw-addmap .button-small');



		chl.bind('click', function(event) {
        var addmaplabel = $('#wp-cw-addmap label');
        var titlelabel = $('.form-required .tit');
        var error = $('.ermaps');
        var greska = 0;

        if(error.length ===1)error.remove();
        if(mapsite.has('.map').length === 0){
		 if(addmaplabel.length === 0){
            addmap.append('<label for="maps" class="ermaps">'+settingsGlobal.errmsg+'</label>');

		}
		 greska = 1;
        }else if(addmaplabel.length !== 0){
            addmap.find('.ermaps').remove();
        }


        if(title.val()) {
	        var inputs = $('[class=roundval]');
	        inputs.val('0');
	        inputs.removeAttr('disabled');
        }else{
			  if(titlelabel.length === 0){
             	title.after('<label for="m_title" class="ermaps tit">'+settingsGlobal.errmsg+'</label>');
            }
             greska = 1;

        }

       /* if(!title.val() || mapsite.has('.map').length === 0){
         event.preventDefault();
        }
		zakomentarisano zbog submit score jer ne radi*/

		var game_dropdown = $('#game_id');

		if(game_dropdown.val() == ""){
			 event.preventDefault();
			 game_dropdown.after('<label for="maps" class="ermaps">'+settingsGlobal.errmsg+'</label>');
			  greska = 1;
		}

		if(settingsGlobal.singular_matches == 'no'){
			if(greska == 1) return false;
		}
        });

        addmap.bind('click', function() {
            var challenge_inputs = $('#mapsite .round input');
            challenge_inputs.prop('readonly', true);
            addmap.find('.ermaps').remove();

            var addround = $('.add-round');
            addround.bind('click', function() {

            var inputs = $('[class=roundval]');
            inputs.val('0');
            inputs.prop('disabled', true);
        });
        });



	}


	 $('#team1').change(function(){
        	var mapa = $('.map');
        	mapa.remove();
            $('#game_id').load(ajaxurl, {"action":"arcane_mutual_games", 'security' : settingsGlobal.security, "team1":$(this).val(), "team2":settingsGlobal.team2_id} );
        });

});


/*composer front editor*/
	function arcane_composer_front_editor(uid) {
    "use strict";
        jQuery.ajax({
        type: 'POST',
        url: ajaxurl,
        dataType:'json',
        data: {"action": "arcane_redirect", idc: uid, 'security' : settingsGlobal.security },
        success: function(data) {
        window.location = settingsGlobal.admin_url +"post.php?vc_action=vc_inline&post_id="+data+"&post_type=team"
        return false;
        }
        });
    }



jQuery( document ).ready(function($) {

	/*Mobile menu parent click with submenu fix*/
	if (jQuery(window).width() < 960) {
	jQuery('.menu-main-container .nav li a').on('click', function(e) {
	    if(!jQuery(this).parent().hasClass('active')) {
	        jQuery('.menu-main-container .nav li').removeClass('active');
	        jQuery(this).parent().addClass('active');
	        e.preventDefault();
	    } else {
	        return true;
	    }
	});

	var logged_info = jQuery(".logged-info a");
	logged_info.addClass('prvi');


	jQuery(document).on('click touchstart', '.logged-info a', function(event) {
	    if(logged_info.hasClass('prvi')) {
	         return false;
	    } else {
	        return true;
	    }
	});


	}

});

jQuery(document).ready(function() {
	if(settingsGlobal.singular_matches == 'yes'){
	    "use strict";
	    var t = jQuery(".add-round"),
	        e = jQuery(".submit-score");
	    t.remove(), e.bind("click", function() {
	        var t = jQuery(".remove");
	        t.remove()
	    })
   }
});

/*RTL VC Full width fix*/
jQuery(document).ready(function() {
	if( jQuery('html').attr('dir') == 'rtl' ){

		function delay_vc(){
			jQuery('[data-vc-full-width="true"]').each( function(i,v){
				var left = jQuery(this).css('left').replace('px','');
				jQuery(this).css('left',left*(-1) );
			});
		}

		setTimeout(delay_vc,500);
	}
});


/**************** Preloader ***************/
jQuery(window).load(function() {
	var preloader = jQuery(".se-pre-con");
	preloader.fadeOut("slow");;
});


/**************** Failed login ***************/
jQuery(document).ready(function() {
	window.$_GET = new URLSearchParams(location.search);
	var value1 = $_GET.get('login');
	var value2 = $_GET.get('captcha');
	var value3 = $_GET.get('captchavalue');

	if( value2 == 'yes'){
		if(!value3)
		NotifyMe(settingsNoty.login_failed_captcha, "error",'','','',2000);
		if( value1 == 'failed')
		NotifyMe(settingsNoty.login_failed, "error",'','','',2000);
	}else if( value1 == 'failed')
		NotifyMe(settingsNoty.login_failed, "error",'','','',2000);
});

/**************arcane_team_image_metabox_callback**********/
jQuery(function($) {

    // the upload image button, saves the id and outputs a preview of the image
 $('.upload_image_button').on('click', function(e) {
    var button = $(this);
    var myuploader = wp.media(
    {
        title : 'Select Image',
        button : {
            text : 'Insert',
        },
        multiple : false
    })

    .on('select', function()
    {
    attachment = myuploader.state().get('selection').first().toJSON();

            if($(button).hasClass("imgid")) {
                $(button).parent('.upload').find('input[type=text]').val(attachment.id);
            } else {
                $(button).parent('.upload').find('input[type=text]').val(attachment.url);
            }
            $(button).parent('.upload').find('img').attr('src', attachment.url);
            $(button).parent('.upload').find('img').show();

    })
    .open(button);

    return false;
});

    // the remove image link, removes the image id from the hidden field and replaces the image preview

    $('.remove_image_button').on('click', function(e) {

            $(this).parent('.upload').find('img').attr('src', '');
            $(this).parent('.upload').find('img').hide();
            $(this).parent('.upload').find('input[type=text]').val('');

        return false;
    });


});



jQuery(document).ready(function() {
	var datep = jQuery('#datepicker_'+settingsGlobal.field_id);
	datep.datepicker({
	      dateFormat: settingsGlobal.date_format,
	      changeMonth: true,
	      changeYear: true,
	      yearRange:  "-100:+0",
	      maxDate: '-1Y',
	      minDate: '-70Y'
	});

	 if(settingsGlobal.dodaj == 'true'){
	     var queryDate = settingsGlobal.date_added;
	     jQuery('#datepicker_'+settingsGlobal.field_id).datepicker({dateFormat: settingsGlobal.date_format});
	     jQuery('#datepicker_'+settingsGlobal.field_id).datepicker('setDate', queryDate);
    }

	var birthday_filed = jQuery('#birthday_field');

     birthday_filed.datepicker({
      dateFormat: settingsGlobal.date_format,
      altFormat: 'yy-mm-dd',
      altField: '#alt-birthday-date',
      changeMonth: true,
	  changeYear: true,
	  yearRange: "-100:+0",
	  maxDate: 'now',
	  minDate: '-70Y'
    });
});


jQuery('#LoginWithAjax_Form').submit(function() {
    if (jQuery.trim(jQuery("#lwa_user_login").val()) === "" || jQuery.trim(jQuery("#lwa_user_pass").val()) === "") {
        NotifyMe(settingsNoty.login_empty, "error");
        return false;
    }

});


/*force numeric values*/
jQuery.fn.forceNumeric = function () {
     return this.each(function () {
         jQuery(this).keydown(function (e) {
             var key = e.which || e.keyCode;

             if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
             // numbers
                 key >= 48 && key <= 57 ||
             // Numeric keypad
                 key >= 96 && key <= 105 ||
             // comma, period and minus, . on keypad
                key == 190 || key == 188 || key == 109 || key == 110 ||
             // Backspace and Tab and Enter
                key == 8 || key == 9 || key == 13 ||
             // Home and End
                key == 35 || key == 36 ||
             // left and right arrows
                key == 37 || key == 39 ||
             // Del and Ins
                key == 46 || key == 45)
                 return true;

             return false;
         });
     });
 }

 jQuery(document).ready(function () {
     let roundVal = jQuery(".roundval");
     roundVal.forceNumeric();

     let wooCardTitle = jQuery('.widget_shopping_cart h2.widgettitle');

     if(wooCardTitle.val() === '')
         wooCardTitle.remove();


 });


