jQuery(document).ready(function ($) {

$("#score_fin").on("click", "a.ajaxsubmitscore", function(event){
    "use strict";
     $.ajax({
        type: "POST",
        url: ajaxurl,
        dataType:"json",
        data: {"action": "arcane_match_score_acc_rej", 'security' : footer_ajax_calls.security,  "req":$(this).attr("data-req"), "mid":$(this).attr("data-mid") },
        success: function(data) {
             if(data == "accepted"){
            NotifyMe(footer_ajax_calls.score_accepted, "information");
           }else if(data == "rejected"){
            NotifyMe(footer_ajax_calls.score_rejected, "information");
           }
          location.reload();
           return false;
        }
    });

});


$("#mtch").on("click", "a.ajaxdeletematch_single", function(){
    "use strict";
     $.ajax({
        type: "POST",
        url: ajaxurl,
        data: {"action": "arcane_match_delete_single", 'security' : footer_ajax_calls.security, "mid":$(this).attr("data-mid")},
        success: function(data) {
           NotifyMe(footer_ajax_calls.match_deleted_request, "information");
           location.reload();
           return false;
        }
    });
});


$("#matches").on("click", "a.ajaxdeletematch", function(event){
    "use strict";
     $.ajax({
        type: "POST",
        url: ajaxurl,
        dataType:"json",
        data: {"action": "arcane_match_delete", 'security' : footer_ajax_calls.security, "req":$(this).attr("data-req"),  "mid":$(this).attr("data-mid")},
        success: function(data) {

        if(data[1] == "accepted"){

           NotifyMe(footer_ajax_calls.delete_accepted, "information" );
           var delete_match = $( ".mj" ).find("[data-mid=" + data[0] + "]");
           delete_match.parent().empty().html(footer_ajax_calls.delete_accepted);

           }else if(data[1] == "rejected"){

           NotifyMe(footer_ajax_calls.delete_rejected, "information");
           var delete_match = $( ".mj" ).find("[data-mid=" + data[0] + "]");
           delete_match.parent().empty().html(footer_ajax_calls.delete_rejected);

           }

           return false;
        }
    });

});


$("#score_fin").on("click", "a.ajaxdeletematchconfirmation", function(event){
    "use strict";
     $.ajax({
        type: "POST",
        url: ajaxurl,
        dataType:"json",
        data: {"action": "arcane_match_delete_confirmation",'security' : footer_ajax_calls.security, "req":$(this).attr("data-req"), "mid":$(this).attr("data-mid")},
        success: function(data) {
          if(data == "accepted"){
           NotifyMe(footer_ajax_calls.match_deleted, "information");
           window.location.replace(footer_ajax_calls.goingto);
          }else if(data == "rejected"){
          	NotifyMe(footer_ajax_calls.match_delete_rejected, "information");
          	 location.reload();
          }

           return false;
        }
    });

});


$("#score_fin").on("click", "a.ajaxloadchlsingle", function(){
    "use strict";
     $.ajax({
        type: "POST",
        url: ajaxurl,
        dataType:"json",
        data: {"action": "arcane_challenge_acc_rej_single", 'security' : footer_ajax_calls.security, "req":$(this).attr("data-req"), "cid":$(this).attr("data-cid")},
        success: function(data) {
           if(data == "accepted"){
           NotifyMe(footer_ajax_calls.challenge_accepted, "information");
           var challenge = $( "#score_fin" );
           challenge.empty().html(footer_ajax_calls.challenge_accepted);

           }else if(data == "rejected"){
           NotifyMe(footer_ajax_calls.challenge_rejected, "information");
           var challenge = $( "#score_fin" );
           challenge.empty().html(footer_ajax_calls.challenge_rejected);

           }
           location.reload();
           return false;
        }
    });
});


$("#matches").on("click", "a.ajaxloadchl", function(event){
    "use strict";
     $.ajax({
        type: "POST",
        url: ajaxurl,
        dataType:"json",
        data: {"action": "arcane_challenge_acc_rej", 'security' : footer_ajax_calls.security, "req":$(this).attr("data-req"), "cid":$(this).attr("data-cid") },
        success: function(data) {

           if(data[1] == "accepted"){

           NotifyMe(footer_ajax_calls.challenge_accepted, "information");
           var challenge = $( ".mj" ).find("[data-cid=" + data[0] + "]");
           challenge.parent().empty().html(footer_ajax_calls.challenge_accepted);

           }else if(data[1] == "rejected"){

           NotifyMe(footer_ajax_calls.challenge_rejected, "information");
           var challenge = $( ".mj" ).find("[data-cid=" + data[0] + "]");
           challenge.parent().empty().html(footer_ajax_calls.challenge_rejected);

           }

           return false;
        }
    });

});


$("#matches").on("click", "a.ajaxloadedit", function(event){
    "use strict";
     $.ajax({
        type: "POST",
        url: ajaxurl,
        dataType:"json",
        data: {"action": "arcane_edit_acc_rej", 'security' : footer_ajax_calls.security, "req":$(this).attr("data-req"), "cid":$(this).attr("data-cid") },
        success: function(data) {
           if(data[1] == "accepted"){

           NotifyMe(footer_ajax_calls.edit_accepted, "information");
           var challenge = $( ".mj" ).find("[data-cid=" + data[0] + "]");
           challenge.parent().empty().html(footer_ajax_calls.edit_accepted);

           }else if(data[1] == "rejected"){

           NotifyMe(footer_ajax_calls.edit_rejected, "information");
           var challenge = $( ".mj" ).find("[data-cid=" + data[0] + "]");
           challenge.parent().empty().html(footer_ajax_calls.edit_rejected);

           }

           return false;
        }
    });

});



$("#score_fin").on("click", "a.ajaxloadeditsingle", function(event){
    "use strict";
     $.ajax({
        type: "POST",
        url: ajaxurl,
        dataType:"json",
        data: {"action": "arcane_edit_acc_rej_single",'security' : footer_ajax_calls.security,  "req":$(this).attr("data-req"), "cid":$(this).attr("data-cid") },
        success: function(data) {
           if(data == "accepted"){

           NotifyMe(footer_ajax_calls.edit_accepted, "information");


           }else if(data == "rejected"){

           NotifyMe(footer_ajax_calls.edit_rejected, "information");


           }
		  location.reload();
           return false;
        }
    });

});


$("#team").one("click", "a.ajaxloadblock", function(event){

    "use strict";
     $.ajax({
        type: "POST",
        url: ajaxurl,
        dataType:"json",
        data: {"action": "arcane_change_membership_block", 'security' : footer_ajax_calls.security, "req": $(this).attr("data-req"), "pid": $(this).attr("data-pid"),"uid": $(this).attr("data-uid") },
        success: function(data) {

          if(data[0] == "join_team"){
            NotifyMe(footer_ajax_calls.join_team, "information");
            var join_button = $(".ajaxloadblock");
            var join_container = $(".team-members-app");
            join_button.remove();
            join_container.append("<div id='score_fin' class='error_msg'>"+footer_ajax_calls.pending_request+"</div>").fadeIn("slow");

           }else if(data[0] == "remove_friend_user"){
            NotifyMe(footer_ajax_calls.remove_friend, "information");
            var leave_button = $(".ajaxloadblock");
            var members_area = $(".team-members-app");
            leave_button.remove();
            members_area.append("<div id='score_fin' class='error_msg'>"+footer_ajax_calls.removed_from_team+"</div>");

           }else if(data[0] == "cancel_request"){
            NotifyMe(footer_ajax_calls.remove_friend, "information");
            var members_area = $(".team-members-app");
            var noti = $("#score_fin");
            var leave_button = $(".ajaxloadblock");
            leave_button.remove();
            noti.remove();
            members_area.append("<div id='score_fin' class='error_msg'>"+footer_ajax_calls.cancel_request+"</div>");

           }
           return false;
        }
    });

});



$("#members-list-fn").on("click", "a.ajaxloadletjoin", function(event){
    "use strict";
         $.ajax({
            type: "POST",
            url: ajaxurl,
            dataType:"json",
            data: {"action": "arcane_change_membership_let_join", 'security' : footer_ajax_calls.security,  "req": $(this).attr("data-req"), "pid": $(this).attr("data-pid"),"uid": $(this).attr("data-uid") },
            success: function(data) {

              if(data[0] == "let_this_member_join"){
                NotifyMe(footer_ajax_calls.let_this_member_join, "information");
                var user = $("."+data[1]+" .member-list-more");
                var user_pen = $("."+data[1]+"");
                var user_pen_text = $("."+data[1]+" .pending-text");
                user.empty().html("<div class='mlm1 mj'>"+footer_ajax_calls.user_joined+"</div>");
                user_pen.removeClass("pending");
                user_pen_text.remove();

               }else if(data[0] == 'member_not_there'){
                    NotifyMe(settingsNoty.member_not_there, "information");
                    $('li.'+data[1]).remove();
                }
              
               if(data[2] == "error"){
               	 NotifyMe(footer_ajax_calls.already_joined, "information");
               	 var user = $("."+data[1]+" .member-list-more");
                var user_pen = $("."+data[1]+"");
                var user_pen_text = $("."+data[1]+" .pending-text");
                user.empty().html("<div class='mlm1 mj'>"+footer_ajax_calls.already_joined+"</div>");
                user_pen.removeClass("pending");
                user_pen_text.remove();
               }

               return false;
            }
        });

});


$("#members-list-fn").on("click", "a.ajaxloadremoveadmin", function(event){
    "use strict";
        $.ajax({
            type: "POST",
            url: ajaxurl,
            dataType:"json",
            data: {"action": "arcane_change_membership_remove_friend_admin",'security' : footer_ajax_calls.security,  "req": $(this).attr("data-req"), "pid": $(this).attr("data-pid"),"uid": $(this).attr("data-uid") },
            success: function(data) {

              if(data[0] == "remove_friend_admin"){
                NotifyMe(footer_ajax_calls.remove_friend, "information");
                var user = $("."+data[1]+" .member-list-more");
                var user_pen = $("."+data[1]+"");
                var user_pen_text = $("."+data[1]+" .pending-text");
                user.empty().html("<div class='mlm1 mj'>"+footer_ajax_calls.remove_friend+"</div>");
                user_pen.removeClass("pending");
                user_pen_text.remove();


               }

               return false;
            }
        });

});


$("#members-list-fn .ajaxloadmakeadmin").on("click", function(event){
    "use strict";
         $.ajax({
            type: "POST",
            url: ajaxurl,
            dataType:"json",
            data: {"action": "arcane_change_membership_make_administrator", 'security' : footer_ajax_calls.security, "req": $(this).attr("data-req"), "pid": $(this).attr("data-pid"),"uid": $(this).attr("data-uid") },
            success: function(data) {

             if(data[0] == "make_administrator"){
                NotifyMe(footer_ajax_calls.make_administrator, "information");
                var user = $("."+data[1]+" .member-list-more");
                user.empty().html("<div class='mlm1 mj'>"+footer_ajax_calls.make_administrator+"</div>");

                }

               return false;
            }
        });
});



$("#members-list-fn .ajaxloaddowngrade").on("click", function(event){
        "use strict";
         $.ajax({
            type: "POST",
            url: ajaxurl,
            dataType:"json",
            data: {"action": "arcane_change_membership_downgrade_to_user",'security' : footer_ajax_calls.security,  "req": $(this).attr("data-req"), "pid": $(this).attr("data-pid"),"uid": $(this).attr("data-uid") },
            success: function(data) {

            if(data[0] == "downgrade_to_user"){
                NotifyMe(footer_ajax_calls.downgrade_to_user, "information");
                var user = $("."+data[1]+" .member-list-more");
                user.empty().html("<div class='mlm1 mj'>"+footer_ajax_calls.downgrade_to_user+"</div>");

                }

               return false;
            }
        });
});


$(".single-team .ajaxdeletebck").on("click", function(event){
    "use strict";
         $.ajax({
            type: "POST",
            url: ajaxurl,
            data: {"action": "arcane_delete_page_background", "pid": $(this).attr("data-pid"), 'security' : footer_ajax_calls.security },
            success: function(data) {
                var single_bck = jQuery("body.single-team");
                var delbck_button = jQuery(".ajaxdeletebck");
                single_bck.attr("style", "background: url('"+footer_ajax_calls.img_url+"')");
                delbck_button.remove();
                NotifyMe(footer_ajax_calls.delete_page_background, "information");
                return false;
            }
        });
});


 $('#myModalDeleteTeam').on('click', 'a.ajaxdeleteteam', function(event){
     "use strict";
     event.preventDefault();
         $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {"action": "arcane_team_delete",'security' : footer_ajax_calls.security, "pid":$(this).attr('data-pid') },
            success: function(data) {
               var modal = $('#myModalDeleteTeam');
               var modalblack = $('.modal-backdrop');
               var vcbar = $('#vc_navbar');
               modal.remove();
               modalblack.remove();
			   vcbar.remove();
               NotifyMe(settingsNoty.team_deleted, "information");
               parent.window.location.href = footer_ajax_calls.home_url;
               return false;
            }
        });

});


$("#resend_activation").on("click", function(event){
    "use strict";
     $.ajax({
        type: "POST",
        url: ajaxurl,
        data: {"action": "arcane_resend_activation_link",'security' : footer_ajax_calls.security, "uid":$(this).attr("data-uid"), "email":$(this).attr("data-email")},
        success: function(data) {
           NotifyMe(footer_ajax_calls.activation_link_sent, "information");
           return false;
        }
    });
});





});